import unittest
from IO.consoleview import ConsoleView
from Logic.basic_gamestate import BasicGamestate

class TestConsoleView(unittest.TestCase):
    
    def test_consoleview(self):
        obj = ConsoleView()
        obj.printhelp()
        obj.printinvalid()
        obj.draw_gamestate(BasicGamestate())
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()