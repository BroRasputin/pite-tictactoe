import unittest
from IO import io_constants
from IO.command_line_input import CommandLineInput

class TestCommandLineInput(unittest.TestCase):
    def test_construct_prompt(self):
            obj = CommandLineInput()
            
            self.assertEqual(obj.construct_prompt(io_constants.SIDE_OS),
                            io_constants.PLAYING_AS.format("O")
                            + " "
                            + io_constants.COMMAND_LINE_PROMPT)
            
            self.assertEqual(obj.construct_prompt(io_constants.SIDE_XS),
                            io_constants.PLAYING_AS.format("X")
                            + " "
                            + io_constants.COMMAND_LINE_PROMPT)
            
            self.assertRaises(AssertionError,obj.construct_prompt,"random string")

    def test_parsecommand(self):
        obj = CommandLineInput()
        self.assertEqual(obj._parsecommand("9"), io_constants.NE)
        self.assertEqual(obj._parsecommand("8"), io_constants.N)
        self.assertEqual(obj._parsecommand("7"), io_constants.NW)
        self.assertEqual(obj._parsecommand("6"), io_constants.E)
        self.assertEqual(obj._parsecommand("5"), io_constants.C)
        self.assertEqual(obj._parsecommand("4"), io_constants.W)
        self.assertEqual(obj._parsecommand("3"), io_constants.SE)
        self.assertEqual(obj._parsecommand("2"), io_constants.S)
        self.assertEqual(obj._parsecommand("1"), io_constants.SW)
        self.assertEqual(obj._parsecommand("help"), io_constants.HELP_COMMAND)
        self.assertEqual(obj._parsecommand("exit"), io_constants.EXIT_COMMAND)
        self.assertEqual(obj._parsecommand("random string sdgdsfg"), io_constants.INVALID_COMMAND)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()