import unittest
from Logic.basic_gamestate import BasicGamestate


class TestBasicGamestate(unittest.TestCase):


    def test_setfield(self):
        obj = BasicGamestate()
        obj.setfield("X",1,1)
        obj.setfield("O",2,2)
        self.assertEquals(obj.board, [[" "," "," "],
                                      [" ","X"," "],
                                      [" "," ","O"]])

    
    def test_setboard(self):
        obj = BasicGamestate()
        board = [["X"," "," "],
                 [" ","X"," "],
                 [" "," ","X"]]
        obj.setboard(board)
        self.assertEquals(obj.board, board)
        
    def test_clearboard(self):
        obj = BasicGamestate()
        board = [["X"," "," "],
                 [" ","X"," "],
                 [" "," ","X"]]
        obj.setboard(board)
        obj.clearboard()
        self.assertEquals(obj.board, [[" "," "," "],
                                      [" "," "," "],
                                      [" "," "," "]])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()