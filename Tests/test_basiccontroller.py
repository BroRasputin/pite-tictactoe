import unittest
from Logic.basic_gamestate import BasicGamestate
from Logic.basiccontroller import BasicController
from IO.consoleview import ConsoleView
from IO.command_line_input import CommandLineInput
from Logic import logic_constants
from IO import io_constants


class TestBasicController(unittest.TestCase):


    def test_check_wincon(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        board = [[" "," ","X"],
                 [" ","X"," "],
                 ["X"," "," "]]
        obj.gamestate.setboard(board)
        self.assertTrue(obj.check_wincon(logic_constants.XS_PLAYER))
        self.assertFalse(obj.check_wincon(logic_constants.OS_PLAYER))
        
        board = [[" ","X"," "],
                 [" ","X"," "],
                 [" ","X"," "]]
        obj.gamestate.setboard(board)
        self.assertTrue(obj.check_wincon(logic_constants.XS_PLAYER))
        self.assertFalse(obj.check_wincon(logic_constants.OS_PLAYER))
        
        board = [[" "," "," "],
                 [" "," "," "],
                 ["X","X","X"]]
        obj.gamestate.setboard(board)
        self.assertTrue(obj.check_wincon(logic_constants.XS_PLAYER))
        self.assertFalse(obj.check_wincon(logic_constants.OS_PLAYER))
        
    def test_checkdraw(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        board = [[" "," ","X"],
                 [" ","X"," "],
                 ["X"," "," "]]
        obj.gamestate.setboard(board)
        self.assertFalse(obj.checkdraw())
        
        board = [["O","X","X"],
                 ["X","O"," O"],
                 ["X","O","X"]]
        obj.gamestate.setboard(board)
        self.assertTrue(obj.checkdraw())
        
    def test_get_symbol_by_player(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertEquals(logic_constants.O_FIELD, obj._getsymbolbyplayer(logic_constants.OS_PLAYER))
        self.assertEquals(logic_constants.X_FIELD, obj._getsymbolbyplayer(logic_constants.XS_PLAYER))
        self.assertRaises(ValueError, obj._getsymbolbyplayer,"randomstring435edg")
        
    def test_get_side_by_player(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertEquals(io_constants.SIDE_OS, obj._getsidebyplayer(logic_constants.OS_PLAYER))
        self.assertEquals(io_constants.SIDE_XS, obj._getsidebyplayer(logic_constants.XS_PLAYER))
        self.assertRaises(ValueError, obj._getsymbolbyplayer,"randomstring435edg")
        
    def test_nextplayer(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertEquals(obj._current_player, obj.players[0])
        obj._nextplayer()
        self.assertEquals(obj._current_player, obj.players[1])
        obj._nextplayer()
        self.assertEquals(obj._current_player, obj.players[0])
        obj._current_player = "randomstring456edfg"
        self.assertRaises(ValueError, obj._nextplayer)
    
    def test_domove(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertRaises(ValueError, obj.domove,"randomstring",1,2)
        self.assertTrue(obj.domove(logic_constants.OS_PLAYER,1,1))
        self.assertFalse(obj.domove(logic_constants.OS_PLAYER,1,1))
        
    def test_processcommand(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertTrue(obj._processcommand(io_constants.N))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.NE))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.NW))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.E))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.C))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.W))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.SW))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.SE))
        obj.gamestate.clearboard()
        self.assertTrue(obj._processcommand(io_constants.S))
        obj.gamestate.clearboard()
        self.assertFalse(obj._processcommand(io_constants.INVALID_COMMAND))
        self.assertFalse(obj._processcommand(io_constants.HELP_COMMAND))
        self.assertFalse(obj._processcommand("randomstringsdf823"))
        
    def test_parsecoords(self):
        obj = BasicController(BasicGamestate(), ConsoleView(), CommandLineInput())
        self.assertEqual(obj._parsecoords(io_constants.NE), [0,2])
        self.assertEqual(obj._parsecoords(io_constants.N), [0,1])
        self.assertEqual(obj._parsecoords(io_constants.NW), [0,0])
        self.assertEqual(obj._parsecoords(io_constants.E), [1,2])
        self.assertEqual(obj._parsecoords(io_constants.C), [1,1])
        self.assertEqual(obj._parsecoords(io_constants.W), [1,0])
        self.assertEqual(obj._parsecoords(io_constants.SE), [2,2])
        self.assertEqual(obj._parsecoords(io_constants.S), [2,1])
        self.assertEqual(obj._parsecoords(io_constants.SW), [2,0])
            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()