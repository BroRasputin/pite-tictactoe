from IO.command_line_input import CommandLineInput
from Logic.basiccontroller import BasicController
from Logic.basic_gamestate import BasicGamestate
from IO.consoleview import ConsoleView

class TicTacToe: 
    def __init__(self, controller):
        self.controller = controller   
        
    def run(self):
        self.controller.rungame()


game = TicTacToe(BasicController(BasicGamestate(), ConsoleView(), CommandLineInput()))
game.run()

