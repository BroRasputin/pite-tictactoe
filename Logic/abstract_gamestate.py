from abc import ABCMeta, abstractproperty, abstractmethod

class AbstractGamestate:
    """Provides an interface for the board on which the game can be played"""
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractproperty
    def board(self):
        pass
    
    @board.setter
    def setboard(self, board):
        pass
    
    @abstractmethod
    def setfield(self, value, x, y):
        """Sets the value of a field at given coordinates"""
        pass
    
    @abstractmethod
    def clearboard(self):
        """Empties the board"""
        pass
