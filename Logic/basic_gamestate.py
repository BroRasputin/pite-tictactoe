from Logic.abstract_gamestate import AbstractGamestate
from Logic.logic_constants import EMPTY_FIELD


class BasicGamestate(AbstractGamestate):
    """Basic gamestate i.e. a 3x3 board"""
    def __init__(self):
        self.setboard([[EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD],
                       [EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD],
                       [EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD]])
        
    def board(self):
        return self.board
    
    def setfield(self, value, x, y,):
        self.board[x][y] = value
        
    def setboard(self, board):
        self.board = board
        
    def clearboard(self):
        self.setboard([[EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD],
                       [EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD],
                       [EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD]])