from abstractcontroller import AbstractController
import logic_constants
import sys
from IO import io_constants
import logging
import datetime
 
logging.basicConfig(filename="TicTacToe.log", level=logging.INFO) 


class BasicController(AbstractController):
    """Basic i.e. two players on a 3x3 board"""

    def __init__(self, gamestate, view, input_):
        super(BasicController, self).__init__(gamestate, view, input_)
        self.players = [logic_constants.OS_PLAYER, logic_constants.XS_PLAYER]
        self._current_player = self.players[0]
        
        

    def check_wincon(self, player):
        """Returns True if a given player has fulfilled the winning conditions and False if they havent"""
        
        symbol = self._getsymbolbyplayer(player)
        
        for i in range(0,3):
            if self._checkrow(i,symbol) == True or self._checkcolumn(i, symbol) == True:
                return True
            
        if self._checkdiagonals(symbol) == True:
            return True
        
        return False
    
    def checkdraw(self):
        """Returns True if the game is a draw due to the board being full"""
        for row in self.gamestate.board:
            for field in row:
                if field == logic_constants.EMPTY_FIELD:
                    return False
        return True
            
    def _checkrow(self, row, symbol):
        """Returns True if all fields in a given row are a given symbol"""
        return all(e == symbol for e in self.gamestate.board[row])
    
    def _checkcolumn(self, column, symbol):
        """Returns True if all fields in a given column are a given symbol"""
        return all(row[column] == symbol for row in self.gamestate.board)
    
    def _checkdiagonals(self, symbol):
        """Returns True if either diagonal has 3 of the same symbol in a row"""        
        if all(self.gamestate.board[i][i] == symbol for i in range (0,3)):
            return True
        if self.gamestate.board[2][0] == symbol and\
                self.gamestate.board[1][1] == symbol and\
                self.gamestate.board[0][2] == symbol:
            return True
        return False
            
    
        
    def _getsymbolbyplayer(self, player):
        """Returns a string which is the symbol used to represent a given player's moves on the board (either O_FIELD or X_FIELD)"""
        if player == logic_constants.OS_PLAYER:
            return logic_constants.O_FIELD
        elif player == logic_constants.XS_PLAYER:
            return logic_constants.X_FIELD
        else:
            logging.error("|{0}|{1}|  ValueError, invalid value of player ({2})".format(datetime.datetime.now(),__name__, player))
            raise ValueError("Invalid value of 'player'. Got: " + player +
                             ". Expected: " + logic_constants.OS_PLAYER 
                             + " or " + logic_constants.XS_PLAYER)
    
    def _getsidebyplayer(self, player):
        """Returns SIDE_OS or SIDE_XS for a given player"""
        if player == logic_constants.OS_PLAYER:
            return io_constants.SIDE_OS
        elif player == logic_constants.XS_PLAYER:
            return io_constants.SIDE_XS
        else:
            logging.error("|{0}|{1}|  ValueError, invalid value of player ({2})".format(datetime.datetime.now(),__name__, player))
            raise ValueError("Invalid value of 'player'. Got: " + player +
                             ". Expected: " + logic_constants.OS_PLAYER 
                             + " or " + logic_constants.XS_PLAYER)        
    
    def _nextplayer(self):
        """Sets the next player as the current player"""
        if self._current_player == self.players[0]:
            self._current_player = self.players[1]
        elif self._current_player == self.players[1]:
            self._current_player = self.players[0]
        else:
            logging.error("|{0}|{1}|  ValueError, invalid value of _current_player ({2})".format(datetime.datetime.now(),__name__, self._current_player))
            raise ValueError("Invalid value of '_current_player'. Got: " + self._current_player +
                                  ". Expected: " + logic_constants.OS_PLAYER 
                                  + " or " + logic_constants.XS_PLAYER)

    def domove(self, player, x, y):
        """Attempts to place a given player's symbol at a given location. Returns True and places the symbol if the move is legal, returns False otherwise"""        
        if self.gamestate.board[x][y] == logic_constants.EMPTY_FIELD:
            if player == logic_constants.OS_PLAYER:
                self.gamestate.setfield(logic_constants.O_FIELD,x,y)
            elif player == logic_constants.XS_PLAYER:
                self.gamestate.setfield(logic_constants.X_FIELD,x,y)
            else:
                logging.error("|{0}|{1}|  ValueError when player {2} making a move at ({3},{4})".format(datetime.datetime.now(),__name__, player,x,y))
                raise ValueError("Invalid value of 'player'. Got: " + player +
                                  ". Expected: " + logic_constants.OS_PLAYER 
                                  + " or " + logic_constants.XS_PLAYER)
            return True
        else:
            return False
        
    def rungame(self):
        """Main game loop"""
        logging.info("|{0}|{1}|  Game started".format(datetime.datetime.now(),__name__))
        while True:
            self._doturn()            
            gameover = False
            
            if self.check_wincon(self._current_player):
                playagain_prompt = io_constants.VICTORY_MSG.format(self._getsidebyplayer(self._current_player))    
                logging.info("|{0}|{1}|  Player {2} wins the game".format(datetime.datetime.now(),__name__,self._current_player))   
                gameover = True 
            elif self.checkdraw():
                logging.info("|{0}|{1}|  The game ends in a draw".format(datetime.datetime.now(),__name__))   
                playagain_prompt = io_constants.DRAW_MSG
                gameover = True                   
            else:
                self._nextplayer()
                
            if gameover:
                playagain = self.input_.getyesno(playagain_prompt)                
                if playagain == io_constants.YES_COMMAND:
                    logging.info("|{0}|{1}|  The players choose to play again".format(datetime.datetime.now(),__name__)) 
                    self._newgame()                    
                    continue
                else:
                    logging.info("|{0}|{1}|  The players choose not to play again".format(datetime.datetime.now(),__name__)) 
                    sys.exit()
        
    def _doturn(self):
        """Asks the currently active player for input until they give a valid move or an exit command"""
        side = self._getsidebyplayer(self._current_player)
        self.view.draw_gamestate(self.gamestate)
        while not self._processcommand(self.input_.getcommand(side)):
            pass
           
        
    def _processcommand(self, command):
        """Takes a command, returns True if it's a valid move command, False if any other command"""
        if command in (io_constants.NW, io_constants.N, io_constants.NE,
                       io_constants.W, io_constants.C, io_constants.E,
                       io_constants.SW,io_constants.S,io_constants.SE):
            coords = self._parsecoords(command)
            if self.domove(self._current_player, coords[0], coords[1]):
                logging.info("|{0}|{1}|  Player {2} makes a legal move at ({3},{4})".format(datetime.datetime.now(),__name__,self._current_player,coords[0],coords[1]))
                return True
            else:
                logging.info("|{0}|{1}|  Player {2} makes an illegal move at ({3},{4})".format(datetime.datetime.now(),__name__,self._current_player,coords[0],coords[1]))
                return False
        elif command == io_constants.HELP_COMMAND:
            self.view.printhelp()
            return False
        elif command == io_constants.INVALID_COMMAND:
            logging.info("|{0}|{1}|  Player {2} issues an illegal command: {3}".format(datetime.datetime.now(),__name__,self._current_player,command))
            self.view.printinvalid()
            return False
        elif command == io_constants.EXIT_COMMAND:
            logging.info("|{0}|{1}|  Player {2} issues an exit command".format(datetime.datetime.now(),__name__,self._current_player))
            sys.exit()
        else:
            self.view.printinvalid()
            return False
            
        
        
    def _parsecoords(self, command):
        """Returns board coordinates for a given move command"""
        return {
                io_constants.NE : [0,2],
                io_constants.N : [0,1],
                io_constants.NW : [0,0],
                io_constants.E : [1,2],
                io_constants.C : [1,1],
                io_constants.W : [1,0],
                io_constants.SE : [2,2],
                io_constants.S : [2,1],
                io_constants.SW : [2,0],              
                }[command]
        
            
    def _newgame(self):
        """Clears the board and gives control to the first player, restarting the game"""
        _current_player = self.players[0]
        self.gamestate.clearboard()
        
        