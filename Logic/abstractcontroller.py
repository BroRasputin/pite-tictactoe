from abc import ABCMeta, abstractmethod

class AbstractController:
    """Provides an interface for a controller, responsible for the game flow and logic"""
    __metaclass__ = ABCMeta

    def __init__(self, gamestate, view, input_):
        self.gamestate = gamestate
        self.view = view
        self.input_ = input_

    @abstractmethod
    def check_wincon(self, gamestate, player):
        """Returns True if a given player has fulfilled the winning conditions and False if they havent"""
        pass
    
    @abstractmethod
    def checkdraw(self):
        """Returns True if the game state means the game is a draw"""
        pass

    @abstractmethod
    def domove(self, player, x, y):
        """Attempts to place a given player's symbol at a given location. Returns True and places the symbol if the move is legal, returns False otherwise"""
        pass

    @abstractmethod
    def rungame(self):
        """Main game loop"""
        pass