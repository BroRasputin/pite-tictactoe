from IO.abstractview import AbstractView
from IO import io_constants

class ConsoleView(AbstractView):
    """A class for displaying the game state in the standard output"""
    def __init__(self):
        super(ConsoleView, self).__init__()
        
    def draw_gamestate(self, gamestate):
        """displays the game state"""
        boardwidth = len(gamestate.board)
        print "-" * boardwidth
        
        for row in gamestate.board:
            buf = ""
            for field in row:
                buf += field
            print buf
            
        print "-" * boardwidth
                    
    def printhelp(self):
        """displays help"""
        print io_constants.HELP_MSG
        
    def printinvalid(self):
        """communicates that a command received was not valid"""
        print io_constants.INVALID_MSG