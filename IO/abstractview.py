
from abc import ABCMeta, abstractmethod

class AbstractView:
    """Provides an interface for displaying the game state"""
    __metaclass__ = ABCMeta

    def __init__(self):
        pass
    
    @abstractmethod
    def draw_gamestate(self, gamestate):
        """displays the game state"""
        pass
        
    @abstractmethod
    def printhelp(self):
        """displays help"""
        pass
    
    @abstractmethod
    def printinvalid(self):
        """communicates that a command received was not valid"""
        pass
