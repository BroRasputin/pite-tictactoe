COMMAND_LINE_PROMPT =   ("Type a number from 1 to 9 "
                        "and press Enter to put your "
                        "symbol in the desired position. "
                        "Type 'help' for an explanation of board numbering. "
                        "Type 'exit' to end the game. ")
PLAYING_AS = "You're playing as {0}s."
SIDE_OS = "O"
SIDE_XS = "X"
VICTORY_MSG = "{0}s win. Play again (y/n)?"
DRAW_MSG = "It's a draw. Play again (y/n)?"

#numpad keys as directions
N = "8"
NE = "9"
E = "6"
SE = "3"
S = "2"
SW = "1"
W = "4"
NW = "7"
C = "5"

DIRECTIONS = ({N : "8",
              NE : "9",
              E : "6",
              SE : "3",
              S : "2",
              SW : "1",
              W : "4",
              NW : "7",
              C : "5",})

HELP_COMMAND = "help"
YES_COMMAND = "y"
NO_COMMAND = "n"
EXIT_COMMAND = "exit"
INVALID_COMMAND = "INVALID_COMMAND"
HELP_MSG = """Fields are numbered as follows:
---
789
456
123
---
"""

INVALID_MSG = "Invalid command!"