from abstractinput import AbstractInput
import io_constants

class CommandLineInput(AbstractInput):
    """An input class for receiving player commands from the console"""
    
    def _construct_prompt(self, side):
        assert(side in (io_constants.SIDE_OS, io_constants.SIDE_XS))  
        return io_constants.PLAYING_AS.format(side)\
               + " "\
               + io_constants.COMMAND_LINE_PROMPT
       
       
    def getcommand(self,side):
        """Takes input from the player and returns a corresponding command value from io_constants"""
        assert(side in (io_constants.SIDE_OS, io_constants.SIDE_XS))
        return self._parsecommand(raw_input(self._construct_prompt(side)))
    
    def getyesno(self, prompt):
        """Returns an answer to a yes/no question"""
        while True:
            answer = raw_input(prompt).lower()
            if answer in (io_constants.YES_COMMAND, io_constants.NO_COMMAND):
                return answer
            
    
     
    def _parsecommand(self, inputstring):
        return {
                "9" : io_constants.NE,
                "8" : io_constants.N,
                "7" : io_constants.NW,
                "6" : io_constants.E,
                "5" : io_constants.C,
                "4" : io_constants.W,
                "3" : io_constants.SE,
                "2" : io_constants.S,
                "1" : io_constants.SW, 
                "help" : io_constants.HELP_COMMAND,
                "exit" : io_constants.EXIT_COMMAND                
                }.get(inputstring, io_constants.INVALID_COMMAND)
        
        
        
