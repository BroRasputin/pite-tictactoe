from abc import ABCMeta, abstractmethod

class AbstractInput:
    """Provides an interface for receiving inputs from the player"""
    __metaclass__ = ABCMeta

    @abstractmethod
    def getcommand(self, side):
        """Takes input from the player and returns a corresponding command value from io_constants"""
        pass

    @abstractmethod
    def getyesno(self, prompt):
        """Returns an answer to a yes/no question"""
        pass
